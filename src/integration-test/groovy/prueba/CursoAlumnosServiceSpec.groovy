package prueba

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class CursoAlumnosServiceSpec extends Specification {

    CursoAlumnosService cursoAlumnosService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new CursoAlumnos(...).save(flush: true, failOnError: true)
        //new CursoAlumnos(...).save(flush: true, failOnError: true)
        //CursoAlumnos cursoAlumnos = new CursoAlumnos(...).save(flush: true, failOnError: true)
        //new CursoAlumnos(...).save(flush: true, failOnError: true)
        //new CursoAlumnos(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //cursoAlumnos.id
    }

    void "test get"() {
        setupData()

        expect:
        cursoAlumnosService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<CursoAlumnos> cursoAlumnosList = cursoAlumnosService.list(max: 2, offset: 2)

        then:
        cursoAlumnosList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        cursoAlumnosService.count() == 5
    }

    void "test delete"() {
        Long cursoAlumnosId = setupData()

        expect:
        cursoAlumnosService.count() == 5

        when:
        cursoAlumnosService.delete(cursoAlumnosId)
        sessionFactory.currentSession.flush()

        then:
        cursoAlumnosService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        CursoAlumnos cursoAlumnos = new CursoAlumnos()
        cursoAlumnosService.save(cursoAlumnos)

        then:
        cursoAlumnos.id != null
    }
}
