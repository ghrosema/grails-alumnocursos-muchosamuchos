package prueba

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class AsistenciaServiceSpec extends Specification {

    AsistenciaService asistenciaService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Asistencia(...).save(flush: true, failOnError: true)
        //new Asistencia(...).save(flush: true, failOnError: true)
        //Asistencia asistencia = new Asistencia(...).save(flush: true, failOnError: true)
        //new Asistencia(...).save(flush: true, failOnError: true)
        //new Asistencia(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //asistencia.id
    }

    void "test get"() {
        setupData()

        expect:
        asistenciaService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Asistencia> asistenciaList = asistenciaService.list(max: 2, offset: 2)

        then:
        asistenciaList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        asistenciaService.count() == 5
    }

    void "test delete"() {
        Long asistenciaId = setupData()

        expect:
        asistenciaService.count() == 5

        when:
        asistenciaService.delete(asistenciaId)
        sessionFactory.currentSession.flush()

        then:
        asistenciaService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Asistencia asistencia = new Asistencia()
        asistenciaService.save(asistencia)

        then:
        asistencia.id != null
    }
}
