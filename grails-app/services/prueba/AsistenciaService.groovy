package prueba

import grails.gorm.services.Service

@Service(Asistencia)
interface AsistenciaService {

    Asistencia get(Serializable id)

    List<Asistencia> list(Map args)

    Long count()

    void delete(Serializable id)

    Asistencia save(Asistencia asistencia)

}