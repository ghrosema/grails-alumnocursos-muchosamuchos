package prueba

import grails.gorm.services.Service

@Service(CursoAlumnos)
interface CursoAlumnosService {

    CursoAlumnos get(Serializable id)

    List<CursoAlumnos> list(Map args)

    Long count()

    void delete(Serializable id)

    CursoAlumnos save(CursoAlumnos cursoAlumnos)

}