package prueba

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class AsistenciaController {

    AsistenciaService asistenciaService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond asistenciaService.list(params), model:[asistenciaCount: asistenciaService.count()]
    }

    def show(Long id) {
        respond asistenciaService.get(id)
    }

    def create() {
        respond new Asistencia(params)
    }

    def save(Asistencia asistencia) {
        if (asistencia == null) {
            notFound()
            return
        }

        try {
            asistenciaService.save(asistencia)
        } catch (ValidationException e) {
            respond asistencia.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'asistencia.label', default: 'Asistencia'), asistencia.id])
                redirect asistencia
            }
            '*' { respond asistencia, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond asistenciaService.get(id)
    }

    def update(Asistencia asistencia) {
        if (asistencia == null) {
            notFound()
            return
        }

        try {
            asistenciaService.save(asistencia)
        } catch (ValidationException e) {
            respond asistencia.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'asistencia.label', default: 'Asistencia'), asistencia.id])
                redirect asistencia
            }
            '*'{ respond asistencia, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        asistenciaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'asistencia.label', default: 'Asistencia'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'asistencia.label', default: 'Asistencia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
