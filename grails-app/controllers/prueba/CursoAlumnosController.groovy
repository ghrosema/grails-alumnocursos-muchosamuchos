package prueba

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CursoAlumnosController {

    CursoAlumnosService cursoAlumnosService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond cursoAlumnosService.list(params), model:[cursoAlumnosCount: cursoAlumnosService.count()]
    }

    def show(Long id) {
        respond cursoAlumnosService.get(id)
    }

    def create() {
        respond new CursoAlumnos(params)
    }

    def save(CursoAlumnos cursoAlumnos) {
        if (cursoAlumnos == null) {
            notFound()
            return
        }

        try {
            cursoAlumnosService.save(cursoAlumnos)
        } catch (ValidationException e) {
            respond cursoAlumnos.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cursoAlumnos.label', default: 'CursoAlumnos'), cursoAlumnos.id])
                redirect cursoAlumnos
            }
            '*' { respond cursoAlumnos, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond cursoAlumnosService.get(id)
    }

    def update(CursoAlumnos cursoAlumnos) {
        if (cursoAlumnos == null) {
            notFound()
            return
        }

        try {
            cursoAlumnosService.save(cursoAlumnos)
        } catch (ValidationException e) {
            respond cursoAlumnos.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'cursoAlumnos.label', default: 'CursoAlumnos'), cursoAlumnos.id])
                redirect cursoAlumnos
            }
            '*'{ respond cursoAlumnos, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        cursoAlumnosService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'cursoAlumnos.label', default: 'CursoAlumnos'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cursoAlumnos.label', default: 'CursoAlumnos'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
